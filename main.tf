module "base-network" {
  source                                      = "cn-terraform/networking/aws"
  version                                     = "2.0.12"
  name_prefix                                 = "test-networking"
  vpc_cidr_block                              = "192.168.0.0/16"
  availability_zones                          = ["us-east-1a", "us-east-1b"]
  public_subnets_cidrs_per_availability_zone  = ["192.168.0.0/19", "192.168.32.0/19"]
  private_subnets_cidrs_per_availability_zone = ["192.168.128.0/19", "192.168.160.0/19"]
}

module "sonar" {
  source              = "cn-terraform/sonarqube/aws"
  name_prefix         = "mflm-poc"
  region              = "us-east-1"
  vpc_id              = module.base-network.vpc_id
  availability_zones  = module.base-network.availability_zones
  public_subnets_ids  = module.base-network.public_subnets_ids
  private_subnets_ids = module.base-network.private_subnets_ids
  db_instance_size    = "db.t3.medium"
}

module "nexus" {
  source              = "cn-terraform/nexus/aws"
  name_prefix         = "mflm-poc"
  region              = "us-east-1"
  vpc_id              = module.base-network.vpc_id
  availability_zones  = module.base-network.availability_zones
  public_subnets_ids  = module.base-network.public_subnets_ids
  private_subnets_ids = module.base-network.private_subnets_ids
}

module "runner" {
  source = "npalm/gitlab-runner/aws"

  aws_region  = "us-east-1"
  environment = "runners-docker"

  enable_runner_ssm_access = true
  runners_use_private_address = false
  enable_eip                  = true

  docker_machine_security_group_description = "Runner docker-machine executor"
  gitlab_runner_security_group_description  = "Gitlab runner"

  vpc_id                   = module.base-network.vpc_id
  subnet_ids_gitlab_runner = module.base-network.public_subnets_ids
  subnet_id_runners        = element(module.base-network.public_subnets_ids, 0)

  runners_executor   = "docker"
  runners_name       = "docker-aws"
  runners_gitlab_url = "https://gitlab.com/"

  gitlab_runner_registration_config = {
    registration_token = "GITLAB_TOKEN"
    runner_name        = "RUNNER_CENTER_AWS"
    tag_list           = "unix_docker"
    description        = "runner docker - auto"
    locked_to_project  = "true"
    run_untagged       = "false"
    maximum_timeout    = "3600"
  }

}
