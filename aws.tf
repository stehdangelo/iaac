provider "aws" {
  region                      = "us-east-1"
  s3_force_path_style         = true
  access_key                  = "AWS_KEY"
  secret_key                  = "AWS_SECRET"
}
